#include "bot.h"
#include <math.h>

double mex;
double mey;

double dist(const void *a, const void *b)
{
  double ax = ((struct food *)a) -> x;
  double ay = ((struct food *)a) -> y;
  double bx = ((struct food *)b) -> x;
  double by = ((struct food *)b) -> y;
  
  double dista = sqrt((mex - ax) * (mex - ax) + (mey - ay) * (mey - ay));
  double distb = sqrt((mex - bx) * (mex - bx) + (mey - by) * (mey - by));
  
  return dista - distb;
  
}

double playerDistance(const void *a, const void *b)
{
  double ax = ((struct player *)a) -> x;
  double ay = ((struct player *)a) -> y;
  double bx = ((struct player *)b) -> x;
  double by = ((struct player *)b) -> y;
  
  double dista = sqrt((mex - ax) * (mex - ax) + (mey - ay) * (mey - ay));
  double distb = sqrt((mex - bx) * (mex - bx) + (mey - by) * (mey - by));
  
  return dista - distb;
  
}

struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;
    
    // Move to the lower-right
    //act.dx = 100;
   // act.dy = 100;
   // act.fire = 0;
   //act.split = 0;
 // action act;
  mex = me.x;
  mey = me.y;
  
  //go to nearest food
  qsort(foods,nfood,sizeof(struct food),dist);
  act.dx = (foods[0].x - me.x);
  act.dy = (foods[0].y - me.y);
 
  
  //Sort players
  qsort(players,nplayers,sizeof(struct player),playerDistance);
  
  //Split when player is near
  if(players[0].x <= 60 && players[0].y <= 60)
  {
      act.split = 1;
      
      //Shoots randomly when they get closer
      if(players[0].x <= 20 && players[0].y <= 20)
      {
            act.fire = 1;
      }
  }
  
  
  
    
    return act;
}